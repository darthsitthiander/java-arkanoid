package com.arkanoid.src.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class InputHandler implements KeyListener {
	
	public InputHandler(Game game){
		game.addKeyListener(this);
		right = new Key();
	}
	
	public class Key{
		private int numTimesPressed = 0;
		public boolean isPressed = false;
		
		public Key(){
			
		}
		
		public int getNumTimesPressed(){
			return numTimesPressed;
		}
		
		public boolean isPressed(){
			return this.isPressed;
		}
		
		public void toggle(boolean isPressed){
			this.isPressed = isPressed;
			if(this.isPressed) numTimesPressed++;
		}
	}
	
	public Key up = new Key();
	public Key down = new Key();
	public Key left = new Key();
	public Key right;
	
	public ArrayList<Key> keys = new ArrayList<Key>();

	@Override
	public void keyPressed(KeyEvent e) {
		toggleKey(e.getKeyCode(),true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggleKey(e.getKeyCode(),false);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void toggleKey(int keyCode, boolean isPressed){
		if(keyCode == KeyEvent.VK_W){
			up.toggle(isPressed);
		}
		if(keyCode == KeyEvent.VK_A){
			left.toggle(isPressed);
		}
		if(keyCode == KeyEvent.VK_S){
			down.toggle(isPressed);
		}
		if(keyCode == KeyEvent.VK_D){
			right.toggle(isPressed);
		}
	}

}
